<?php $title = 'Location'; ?>

<?php
require('../layouts/header.php');

is_admin();

if (isset($_GET['id'])) {
    $no = 1;
    $id = $_GET['id'];

    $lokasi = find('lokasi', $id);

    if (isset($_GET['date'])) {
        $date = $_GET['date'];

        $visitor = mysqli_query($connection, "SELECT * FROM visitor JOIN user ON visitor.user_id = user.id WHERE lokasi_id = $id AND DATE(waktu_checkin) = '$date' ");
    } else {
        $visitor = mysqli_query($connection, "SELECT * FROM visitor JOIN user ON visitor.user_id = user.id WHERE lokasi_id = $id ");
    }
}


?>


<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <h2 class="mb-3">Detail Visitor <?= $lokasi['nama_lokasi'] ?></h2>

            <form action="" method="get">
                <input type="hidden" name="id" value="<?= $lokasi['id'] ?>">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input type="date" name="date" id="date" class="form-control" value="<?= isset($_GET['date']) ? $_GET['date'] : '' ?>">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <br>
                            <button type="submit" class="btn btn-primary mt-2">Tampil</button>
                        </div>
                    </div>
                </div>
            </form>


            <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th>Email</th>
                        <th>Nama</th>
                        <th>Waktu Check In</th>
                        <th>Waktu Check Out</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($visitor as $visit) : ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $visit['email'] ?></td>
                            <td><?= $visit['nama'] ?></td>
                            <td><?= $visit['waktu_checkin'] ?? '-' ?></td>
                            <td><?= $visit['waktu_checkout'] ?? '-' ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

<?php require('../layouts/footer.php') ?>