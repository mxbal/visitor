<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="assets/img/logo/logo.png" rel="icon">
    <title>RuangAdmin - Blank Page</title>
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/ruang-admin.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <div id="wrapper">

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <!-- TopBar -->
                <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
                    <div class="container">
                        <a href="" class="text-white">
                            <img src="" alt=""> Sistem Monitoring Pengunjung Menggunakan 2 Faktor Autentikasi
                        </a>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    Home
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="login.php">
                                    Login
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="register.php">
                                    Register
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- Topbar -->

                <!-- Container Fluid-->
                <div class="container" id="container-wrapper">
                    <div class="row text-center mb-5">
                        <div class="col-md-12">
                            <h1>Sistem Monitoring Pengunjung Menggunakan 2 Faktor Autentikasi</h1>
                            <p>
                                Sesuai namanya Sistem Monitoring Pengunjung Menggunakan 2 Faktor Otentikasi, sistem akan melakukan verifikasi data diri pengunjung menggunakan 2 faktor, yaitu NIK dan wajah. Setelah pengunjung memasukkan NIK pada web yang disediakan di lokasi wisata, kamera yang dipasang di pintu masuk akan melakukan scan wajah pada pengunjung. Hasil scan tersebut kemudian akan diidentifikasi, disimpan dan diolah menggunakan library OpenCV agar dapat dicocokkan dengan wajah yang digunakan saat melakukan registrasi. Bila data yang diterima sesuai, maka secara otomatis web akan menampilkan keterangan jumlah pengunjung yang bertambah beserta dengan nama lokasi, jam masuk, dan jam keluar pengunjung tersebut.
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="text-center">Fitur</h1>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-center">Sistem Monitoring</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-center">Face Recognition</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-center">Principal Components Analysis</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-center">Web Based App</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-center">MySQL</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-center">OpenCV</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!---Container Fluid-->
            </div>
        </div>
    </div>

    <!-- Scroll to top -->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="assets/js/ruang-admin.min.js"></script>

</body>

</html>