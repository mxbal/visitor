-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 13, 2022 at 04:41 PM
-- Server version: 5.7.34
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `visitor`
--

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `id` int(11) NOT NULL,
  `lokasi_id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `mode` varchar(10) NOT NULL DEFAULT 'SCAN'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`id`, `lokasi_id`, `nama`, `mode`) VALUES
(1, 2, 'Rumah Makan Encok', 'SCAN'),
(2, 3, 'Dago Sari euy', 'SCAN');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(11) NOT NULL,
  `nama_lokasi` varchar(128) NOT NULL,
  `alamat` text NOT NULL,
  `jam_operasional` varchar(50) NOT NULL,
  `jml_maximum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id`, `nama_lokasi`, `alamat`, `jam_operasional`, `jml_maximum`) VALUES
(2, 'Cicaheum', 'Bandung', '02:39 - 22:40', 5),
(3, 'Dago', 'Bandung', '03:40 - 23:40', 10);

-- --------------------------------------------------------

--
-- Table structure for table `nik`
--

CREATE TABLE `nik` (
  `id` int(11) NOT NULL,
  `nik` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `secret_key`
--

CREATE TABLE `secret_key` (
  `id` int(11) NOT NULL,
  `secret_key` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `secret_key`
--

INSERT INTO `secret_key` (`id`, `secret_key`) VALUES
(1, 'visitor2022');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `gambar_wajah` varchar(128) DEFAULT NULL,
  `gambar_ktp` varchar(128) DEFAULT NULL,
  `level` enum('admin','visitor') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nik`, `nama`, `email`, `password`, `gambar_wajah`, `gambar_ktp`, `level`) VALUES
(1, '12345', 'Developer', 'dev@gmail.com', '$2a$12$we6zltvOkK8LD2W84fkvXu7nfx0wXnzMPWLm1Dkk3fZqVnZbSMWGq', 'admin.png', NULL, 'admin'),
(2, '4444', 'Esa Nuraida', 'esanuraida@gmail.com', '$2a$12$we6zltvOkK8LD2W84fkvXu7nfx0wXnzMPWLm1Dkk3fZqVnZbSMWGq', 'esa_nuraida.png', '2810.png', 'visitor'),
(3, '1231', 'Sandy Nugroho', 'sandy@gmail.com', '$2y$10$9fCvH8kgVEauyveSXZkPmeHIpCsU8FiNp.bzkdTaEw6Ee8Usd0o0W', 'sandy_nugroho.jpeg', '1231.jpeg', 'visitor'),
(4, '112233', 'Putri', 'putri@gmail.com', '$2a$12$we6zltvOkK8LD2W84fkvXu7nfx0wXnzMPWLm1Dkk3fZqVnZbSMWGq', 'safdsf.jpeg', '2341234.jpeg', 'visitor');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lokasi_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `checkin` int(11) NOT NULL DEFAULT '0',
  `waktu_checkin` varchar(50) DEFAULT NULL,
  `foto_checkin` varchar(128) DEFAULT NULL,
  `checkout` int(11) NOT NULL DEFAULT '0',
  `waktu_checkout` varchar(50) DEFAULT NULL,
  `foto_checkout` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `visitor`
--

INSERT INTO `visitor` (`id`, `user_id`, `lokasi_id`, `device_id`, `checkin`, `waktu_checkin`, `foto_checkin`, `checkout`, `waktu_checkout`, `foto_checkout`) VALUES
(2, 2, 1, 1, 1, '2022-04-08 22:07:42', '', 1, '2022-04-08 22:07:53', ''),
(3, 1, 1, 1, 1, '2022-04-08 22:08:42', '', 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nik`
--
ALTER TABLE `nik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `secret_key`
--
ALTER TABLE `secret_key`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nik`
--
ALTER TABLE `nik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `secret_key`
--
ALTER TABLE `secret_key`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
